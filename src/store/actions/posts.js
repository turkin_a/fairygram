import axios from '../../axios-api';
import { GET_ALL_POSTS_SUCCESS } from "./actionTypes";

export const getAllPosts = () => {
	return dispatch => {
		return axios('/images').then(response => {
			dispatch(getAllPostSuccess(response.data));
		})
	}
};

const getAllPostSuccess = posts => {
	return {type: GET_ALL_POSTS_SUCCESS, posts};
};