import { createStackNavigator } from "react-navigation";
import LoginScreen from "../components/LoginScreen/LoginScreen";
import RegisterScreen from "../components/RegisterScreen/RegisterScreen";

const AuthNavigator = createStackNavigator(
	{
		Login: {
			screen: LoginScreen,
			navigationOptions: {
				header: null
			}
		},
		Register: {
			screen: RegisterScreen,
			navigationOptions: {
				header: null
			}
		}
	},
	{
		initialRouteName: 'Login'
	}
);

export default AuthNavigator;
