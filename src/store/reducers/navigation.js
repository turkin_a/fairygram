import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../../navigation/AppWithNavigationState';
import { LOGIN_USER_SUCCESS, REGISTER_USER_SUCCESS, USER_LOGGED_IN, USER_LOGGED_OUT } from "../actions/actionTypes";

// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = AppNavigator.router.getActionForPathAndParams('Login');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const secondAction = AppNavigator.router.getActionForPathAndParams('Register');
const thirdAction = AppNavigator.router.getActionForPathAndParams('Home');
const initialNavState = AppNavigator.router.getStateForAction(
	firstAction,
	tempNavState,
	secondAction,
	thirdAction
);

const reducer = (state = initialNavState, action) =>  {
	let nextState;
	switch (action.type) {
		case 'Back':
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.back(),
				state
			);
		case 'Register':
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({routeName: 'Register'}),
				state
			);
		case USER_LOGGED_OUT:
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({routeName: 'Login'}),
				state
			);
		case USER_LOGGED_IN:
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({routeName: 'Camera'}),
				state
			);
		case 'TakePhoto':
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({routeName: 'TakePhoto'}),
				state
			);
		default:
			nextState = AppNavigator.router.getStateForAction(action, state);
			break;
	}
	
	// Simply return the original `state` if `nextState` is null or undefined.
	return nextState || state;
};

export default reducer;